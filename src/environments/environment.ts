// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCG_BEFSHEfSskMkC3yNCCaryCS9hoVY3s',
    authDomain: 'memory-game-8.firebaseapp.com',
    databaseURL: 'https://memory-game-8.firebaseio.com',
    projectId: 'memory-game-8',
    storageBucket: 'memory-game-8.appspot.com',
    messagingSenderId: '1032298290825'
  },
  contentful: {
    contentfulSpace: 'x5vy00ucea0c',
    contentfulAccessToken: '6f07915e804bc721499b55e409dd2bad760bae276264dd20e3c7cc5f3d0c4ea6',
    contentfulEnvironment: 'master'
  }
};
