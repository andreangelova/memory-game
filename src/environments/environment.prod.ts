export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCG_BEFSHEfSskMkC3yNCCaryCS9hoVY3s',
    authDomain: 'memory-game-8.firebaseapp.com',
    databaseURL: 'https://memory-game-8.firebaseio.com',
    projectId: 'memory-game-8',
    storageBucket: 'memory-game-8.appspot.com',
    messagingSenderId: '1032298290825'
  },
  contentful: {
    contentfulSpace: 'x5vy00ucea0c',
    contentfulAccessToken: '6f07915e804bc721499b55e409dd2bad760bae276264dd20e3c7cc5f3d0c4ea6',
    contentfulEnvironment: 'master'
  }
};
