import {ScoreBoardActionTypes, All} from './scoreBoard.action';

const initialState: ScoreBoardState = {
};
export interface ScoreBoardState {
  scoreBoard?: any[];
  loading?: boolean;
  message?: {
    title: string,
    text: string
  };
}

export function ScoreBoardReducer(state = initialState, action: All): ScoreBoardState {
  switch (action.type) {
    case ScoreBoardActionTypes.LIST_SCOREBOARD:
      return {
        ...state,
        loading: true,
        message: null
      };
    case ScoreBoardActionTypes.UPDATE_SCOREBOARD:
      action.payload.scoreBoard = state.scoreBoard; // adding the scoreBoard to the service to make a calculation
      return {
        ...state,
        loading: true,
        message: null
      };

    case ScoreBoardActionTypes.LIST_SCOREBOARD_FAIL:
    case ScoreBoardActionTypes.UPDATE_SCOREBOARD_FAIL:
      return {
        ...state,
        loading: false,
        message: {
          title: action.payload.status,
          text: action.payload.statusText
        }
      };

    case ScoreBoardActionTypes.LIST_SCOREBOARD_SUCCESS:
    case ScoreBoardActionTypes.UPDATE_SCOREBOARD_SUCCESS:
      return {
        ...state,
        loading: false,
        message: null,
        scoreBoard: JSON.parse(action.payload)
      };
    default :
      return state;
  }
}
