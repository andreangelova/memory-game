import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from 'rxjs';
import * as ScoreBoardAction from './scoreBoard.action';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {ScoreBoardSerivce} from '../../Services/scoreBoard.service';


@Injectable()
export class ScoreBoardEffects {
  constructor(private actions$: Actions, private scoreBoardSerivce: ScoreBoardSerivce) {}

  @Effect()
  ListScoreBoard: Observable<any> = this.actions$.pipe(
    ofType(ScoreBoardAction.ScoreBoardActionTypes.LIST_SCOREBOARD),
    mergeMap(action =>
      this.scoreBoardSerivce.getScoreBoard((<any>action).payload).then(
        scores => new ScoreBoardAction.ListScoreBoardSuccess(scores),
        reject => new ScoreBoardAction.ListScoreBoardSuccess(reject)
      )
    )
  );

  @Effect()
  UpdateScoreBoard: Observable<any> = this.actions$.pipe(
    ofType(ScoreBoardAction.ScoreBoardActionTypes.UPDATE_SCOREBOARD),
    mergeMap(action =>
      this.scoreBoardSerivce.updateScoreBoard((<any>action).payload).then(
        scores => new ScoreBoardAction.UpdateScoreBoardSuccess(scores),
        reject => new ScoreBoardAction.UpdateScoreBoardFail(reject)
      )
    )
  );
}
