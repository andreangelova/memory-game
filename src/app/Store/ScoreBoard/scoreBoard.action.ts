import {Action} from '@ngrx/store';

export enum ScoreBoardActionTypes {
  LIST_SCOREBOARD = 'LIST_SCOREBOARD',
  LIST_SCOREBOARD_SUCCESS = 'LIST_SCOREBOARD_SUCCESS',
  LIST_SCOREBOARD_FAIL = 'LIST_SCOREBOARD_FAIL',
  UPDATE_SCOREBOARD = 'UPDATE_SCOREBOARD',
  UPDATE_SCOREBOARD_SUCCESS = 'UPDATE_SCOREBOARD_SUCCESS',
  UPDATE_SCOREBOARD_FAIL = 'UPDATE_SCOREBOARD_FAIL'
}

export  class ListScoreBoard implements Action {
  readonly type = ScoreBoardActionTypes.LIST_SCOREBOARD;
  constructor(public payload: any) {}
}
export  class ListScoreBoardSuccess implements Action {
  readonly type = ScoreBoardActionTypes.LIST_SCOREBOARD_SUCCESS;
  constructor(public payload: any) {}
}
export  class ListScoreBoardFail implements Action {
  readonly type = ScoreBoardActionTypes.LIST_SCOREBOARD_FAIL;
  constructor(public payload: any) {}
}
export  class UpdateScoreBoard implements Action {
  readonly type = ScoreBoardActionTypes.UPDATE_SCOREBOARD;
  constructor(public payload: any) {}
}
export  class UpdateScoreBoardSuccess implements Action {
  readonly type = ScoreBoardActionTypes.UPDATE_SCOREBOARD_SUCCESS;
  constructor(public payload: any) {}
}
export  class UpdateScoreBoardFail implements Action {
  readonly type = ScoreBoardActionTypes.UPDATE_SCOREBOARD_FAIL;
  constructor(public payload: any) {}
}

export type All =
  | ListScoreBoard
  | ListScoreBoardSuccess
  | ListScoreBoardFail
  | UpdateScoreBoard
  | UpdateScoreBoardSuccess
  | UpdateScoreBoardFail;
