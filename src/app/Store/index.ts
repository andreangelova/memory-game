import {ActionReducerMap} from '@ngrx/store';
import {AppStore} from '../Models/AppStore.model';
import {GameReducer} from './Game/game.reducer';
import {ScoreBoardReducer} from './ScoreBoard/scoreBoard.reducer';

export const reducers: ActionReducerMap<AppStore> = {
    games: GameReducer,
    scoreBoard: ScoreBoardReducer
};
