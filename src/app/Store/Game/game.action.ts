import {Action} from '@ngrx/store';

export enum GameActionTypes {
  START_GAME = 'START_GAME',
  UPDATE_GAME = 'UPDATE_GAME',
  UPDATE_GAME_SUCCESS = 'UPDATE_GAME_SUCCESS',
  ADD_IMAGE = 'ADD_IMAGE',
  ADD_IMAGE_SUCCESS = 'ADD_IMAGE_SUCCESS',
  ADD_IMAGE_FAIL = 'ADD_IMAGE_FAIL',
  CHANGE_TYPE_OF_GAME = 'CHANGE_TYPE_OF_GAME'
}

export  class StartGame implements Action {
  readonly type = GameActionTypes.START_GAME;
  constructor(public payload: any) {}
}
export  class UpdateGame implements Action {
  readonly type = GameActionTypes.UPDATE_GAME;
  constructor(public payload: any) {}
}
export  class UpdateGameSuccess implements Action {
  readonly type = GameActionTypes.UPDATE_GAME_SUCCESS;
  constructor(public payload: any) {}
}
export  class AddImage implements Action {
  readonly type = GameActionTypes.ADD_IMAGE;
  constructor(public payload: any) {}
}
export  class AddImageSuccess implements Action {
  readonly type = GameActionTypes.ADD_IMAGE_SUCCESS;
  constructor(public payload: any) {}
}
export  class AddImageFail implements Action {
  readonly type = GameActionTypes.ADD_IMAGE_FAIL;
  constructor(public payload: any) {}
}
export  class ChangeTypeOfGame implements Action {
  readonly type = GameActionTypes.CHANGE_TYPE_OF_GAME;
  constructor(public payload: any) {}
}

export type All =
  | StartGame
  | UpdateGame
  | UpdateGameSuccess
  | AddImage
  | AddImageSuccess
  | AddImageFail
  | ChangeTypeOfGame;
