import {GameModel} from '../../Models/Game.model';

export class InitialState {
  games = [
    {
      level: 1,
      size: 4,
      score: 4 * 4,
      game: // this.generateArrayOfImages('animal', (4 * 4) / 2)
      [
        {'id': '6oImWc85Xw0iRY1lVtzsYe'}, {'id': '6oImWc85Xw0iRY1lVtzsYe'},
        {'id': '3PpSXR9qGEDPGbsDo9s3zZ'}, {'id': '3PpSXR9qGEDPGbsDo9s3zZ'},
        {'id': '7r3gdFIMf1sOLkouDi46gC'}, {'id': '7r3gdFIMf1sOLkouDi46gC'},
        {'id': '5rEui8vSIYlA0l56LQQsT3'}, {'id': '5rEui8vSIYlA0l56LQQsT3'},
        {'id': '3Zxpt9FCWPDofRCCZVPHmy'}, {'id': '3Zxpt9FCWPDofRCCZVPHmy'},
        {'id': '7imFpA5rsPWFl7M6CKG4bR'}, {'id': '7imFpA5rsPWFl7M6CKG4bR'},
        {'id': '58hV8r7mHHp02ZzMDCpCd0'}, {'id': '58hV8r7mHHp02ZzMDCpCd0'},
        {'id': '29PeEynfwyNNYa8GM01SaW'}, {'id': '29PeEynfwyNNYa8GM01SaW'}
      ]
    },

    {
      level: 2,
      size: 6,
      score: 6 * 6,
      game: this.generateArrayOfImages('flower', (6 * 6) / 2)
    },

    {
      level: 3,
      size: 8,
      score: 8 * 8,
      game: this.generateArrayOfImages('logo', (8 * 8) / 2)
    },

    {
      level: 4,
      size: 10,
      score: 10 * 10,
      game: this.generateArrayOfImages('food', (10 * 10) / 2)
    },

    {
      level: 5,
      size: 12,
      score: 12 * 12,
      game: this.generateArrayOfImages('flag', (12 * 12) / 2)
    }
  ];
  finalScore = 0;
  activeGame;
  contentfulImages = [];
  typeOfGame = {id: 1, name: 'Normal mode'};
  gameUpdated = false;
  constructor() {}

  generateArrayOfImages(text, number) {
    const array = [];
    for (let i = 1 ; i <= number; i ++) {
      array.push({'img': text + i + '.png'});
      array.push({'img': text + i + '.png'});
    }
    return array;
  }
}

export interface GameState {
  loading?: boolean;
  games: any[];
  activeGame?: {
    game: GameModel[],
    level: number,
    size: number,
    score: number
  };
  typeOfGame: {
    id: number,
    name: string
  };
  finalScore: number;
  contentfulImages: any[];
  gameUpdated: boolean;
}
