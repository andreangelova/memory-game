import {All, GameActionTypes} from './game.action';
import {InitialState, GameState} from './game.state';

export function GameReducer(state = new InitialState(), action: All): GameState {
  switch (action.type) {
    case GameActionTypes.START_GAME:
      const initialSate = new InitialState();
      const selectedGame = initialSate.games[action.payload];
      selectedGame.game = shuffleArray(selectedGame.game);
      selectedGame.game = makeTheArrayMatrix(selectedGame.game, selectedGame.size);
      action.payload === 0 ? state.finalScore = 0 : state.finalScore += state.activeGame.score;
      for (let i = 0; i < state.contentfulImages.length; i++) {
        selectedGame.game = mapContenfulImages(selectedGame.game, state.contentfulImages[i]);
      }
      return {
        ...state,
        activeGame: selectedGame,
        contentfulImages: state.contentfulImages,
        typeOfGame: state.typeOfGame ? state.typeOfGame : initialSate.typeOfGame,
        loading: false,
        gameUpdated: false
      };
    case GameActionTypes.UPDATE_GAME_SUCCESS:
      state.activeGame.game = action.payload.game;
      state.activeGame.score = action.payload.score;
      return {
        ...state,
        activeGame: state.activeGame,
        loading: false,
        gameUpdated: true
      };

    case GameActionTypes.ADD_IMAGE:
    case GameActionTypes.UPDATE_GAME:
      return {
        ...state,
        loading: true,
        gameUpdated: false
      };
    case GameActionTypes.ADD_IMAGE_SUCCESS:
      state.contentfulImages.push(action.payload);
      state.activeGame.game = mapContenfulImages(state.activeGame.game, action.payload);
      return {
        ...state,
        loading: false,
        activeGame: state.activeGame,
        contentfulImages: state.contentfulImages,
        gameUpdated: false
      };
    case GameActionTypes.CHANGE_TYPE_OF_GAME:
      return {
        ...state,
        typeOfGame: action.payload,
        gameUpdated: false
      };
    default :
      return state;
  }
}

function shuffleArray(array) {
  for (let i = 0; i < array.length - 1; i++) {
    let j = i + Math.floor(Math.random() * (array.length - i));
    let temp = array[j];
    array[j] = array[i];
    array[i] = temp;
  }
  return array;
}

function makeTheArrayMatrix(array, numberOfRows) {
  let newArray = [];
  let countRow = - 1, countColum = 0;
  for (let i = 0; i < array.length; i++) {
    if (i % numberOfRows === 0) {
      countRow++;
      countColum = 0;
      newArray[countRow] = [];
    } else {
      countColum++;
    }
    newArray[countRow][countColum] = array[i];
  }
  return newArray;
}

function mapContenfulImages(matrix, imageData) {
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      if (matrix[i][j].id === imageData.id) {
        matrix[i][j].contentfulImg = imageData.contentfulImg;
      }
    }
  }
  return matrix;
}

