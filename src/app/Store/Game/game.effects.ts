import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable} from 'rxjs';
import * as GameAction from './game.action';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {ContentfulService} from '../../Services/contentful.service';
import {GameSerivce} from '../../Services/game.service';
import 'rxjs/add/observable/of';


@Injectable()
export class GameEffects {
  constructor(private actions$: Actions, private contentfulService: ContentfulService, private gameSerivce: GameSerivce) {}

  @Effect({dispatch: false})
  StartGame: Observable<any> = this.actions$.pipe(
    ofType(GameAction.GameActionTypes.START_GAME)
  );

  @Effect()
  UpdateGame: Observable<any> = this.actions$.pipe(
    ofType(GameAction.GameActionTypes.UPDATE_GAME),
    mergeMap(action =>
      this.gameSerivce.updateGame((<any>action).payload).then(
        game => new GameAction.UpdateGameSuccess(game)
      )
    )
  );

  @Effect()
  AddImage: Observable<any> = this.actions$.pipe(
    ofType(GameAction.GameActionTypes.ADD_IMAGE),
    mergeMap(action =>
      this.contentfulService.getAssets((<any>action).payload).then(
        data => new GameAction.AddImageSuccess(data),
        reject => new GameAction.AddImageFail(reject)
      )
    )
  );

  @Effect()
  ChangeTypeOfGame: Observable<any> = this.actions$.pipe(
    ofType(GameAction.GameActionTypes.CHANGE_TYPE_OF_GAME),
    mergeMap(() => Observable.of(new GameAction.StartGame(0))) // start the game from the beginning after changing the type
  );
}
