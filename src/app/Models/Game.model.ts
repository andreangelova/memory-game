export class GameModel {
  img: string;
  open?: boolean;
  match?: boolean;
  id?: string;

  constructor(
    img: string,
    open: boolean,
    match: boolean,
    id: string
  ) {
    this.img = img;
    this.open = open;
    this.match = match;
    this.id = id;
  }
}
