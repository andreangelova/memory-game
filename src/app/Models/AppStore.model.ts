import {GameState} from '../Store/Game/game.state';
import {ScoreBoardState} from '../Store/ScoreBoard/scoreBoard.reducer';

export interface AppStore {
  games: GameState;
  scoreBoard: ScoreBoardState;
}
