import { BrowserModule } from '@angular/platform-browser';
import { NgModule, forwardRef } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { GameComponent } from './Components/game/game.component';
import { ScoreboardComponent } from './Components/scoreboard/scoreboard.component';
import { MenuComponent } from './Components/menu/menu.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {StoreModule} from '@ngrx/store';
import {reducers} from './Store';
import {EffectsModule} from '@ngrx/effects';
import {GameEffects} from './Store/Game/game.effects';
import {MatDialogModule} from '@angular/material';
import { InfoDialogComponent } from './Components/dialogs/info-dialog/info-dialog.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { ScoreBoardSerivce } from './Services/scoreBoard.service';
import {ScoreBoardEffects} from './Store/ScoreBoard/scoreBoard.effects';
import {HttpClientModule} from '@angular/common/http';
import { EnterNameDialogComponent } from './Components/dialogs/enter-name-dialog/enter-name-dialog.component';
import { FormsModule } from '@angular/forms';
import { ContentfulService } from './Services/contentful.service';
import { GameInfoComponent } from './Components/game/game-info/game-info.component';
import { GameSerivce } from './Services/game.service';
import { DialogSerivce } from './Services/dialog.service';


@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    ScoreboardComponent,
    MenuComponent,
    InfoDialogComponent,
    EnterNameDialogComponent,
    GameInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([GameEffects, ScoreBoardEffects]),
    MatDialogModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    HttpClientModule,
    FormsModule
  ],
  entryComponents: [
    InfoDialogComponent, EnterNameDialogComponent, GameInfoComponent
  ],
  providers: [forwardRef(() => ScoreBoardSerivce), ContentfulService, GameSerivce, DialogSerivce],
  bootstrap: [AppComponent]
})
export class AppModule { }
