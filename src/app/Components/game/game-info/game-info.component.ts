import { Component, OnInit, Input } from '@angular/core';
import {AppStore} from '../../../Models/AppStore.model';
import {Store} from '@ngrx/store';
import {ChangeTypeOfGame} from '../../../Store/Game/game.action';

@Component({
  selector: 'app-game-info',
  templateUrl: './game-info.component.html',
  styleUrls: ['./game-info.component.css']
})
export class GameInfoComponent implements OnInit {

  @Input() level;
  @Input() score;
  @Input() typeOfGame;
  typesOfGame = [
    {id: 1, name: 'Normal mode'},
    {id: 2, name: 'Endless mode'}
  ];

  constructor(private store: Store<AppStore>) { }

  ngOnInit() {
  }

  changeTypeOfGame(selectedType) {
    if (this.typeOfGame.id !== selectedType.id) { // no need to change the game if it's the same type
      this.store.dispatch(new ChangeTypeOfGame(selectedType));
    }
  }

}
