import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {AppStore} from '../../Models/AppStore.model';
import {Store} from '@ngrx/store';
import {StartGame, UpdateGame} from '../../Store/Game/game.action';
import {DialogSerivce} from '../../Services/dialog.service';
import {GameSerivce} from '../../Services/game.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  game;
  score;
  level;
  games;
  finalScore;
  typeOfGame;
  constructor(private spinner: NgxSpinnerService, private store: Store<AppStore>, public dialog: DialogSerivce,
              private gameSerivce: GameSerivce) {
    this.store.select(res => res.games).subscribe(res => {
      if (res.activeGame) {
        this.game = res.activeGame.game;
        this.score = res.activeGame.score;
        this.level = res.activeGame.level;
        this.games = res.games;
        this.finalScore = res.finalScore;
        this.typeOfGame = res.typeOfGame;
        if (res.gameUpdated) {
          this.checkIfGameIsDone();
          if (res.activeGame.score === 0 && res.typeOfGame.id === 2) { // if the score is 0 and it's endless mode end the game
            this.dialog.openDialogAndAfterCloseStartGame({header: 'Darn it!', body: 'Better luck next time.', button: 'Try again'}, 0);
          }
        }
      }
      res.loading ? this.spinner.show() : this.spinner.hide();
    });
  }

  ngOnInit() {
    if (!this.game) {
      this.store.dispatch(new StartGame(0));
    }
  }

  openCard(row, colum) { // if it's open then that means it clicked on the same tile and if it's matched then do nothing
    if (this.game[row][colum]['open'] !== true && this.game[row][colum]['match'] !== true) {
      this.store.dispatch(new UpdateGame({game: this.game, score: this.score, row: row, colum: colum, typeOfGame: this.typeOfGame}));
    }
  }

  checkIfGameIsDone() {
    if (this.gameSerivce.checkIfGameIsDone(this.game)) {
      if (this.games.length > this.level) { // there are still some more games to play
        this.dialog.openDialogAndAfterCloseStartGame({header: 'Great Job', body: 'You finished this level, get ready for the next!',
          button: 'Ready'}, this.level);
      } else { // game successfuly finished
        if (this.typeOfGame.id === 1) { // enter username
          this.dialog.openDialogEnterUserName(this.finalScore + this.score);
        } else { // end of endless game
          this.dialog.openDialogAndAfterCloseStartGame({header: 'Well done master!',
            body: 'Such an amazing skills, care to try again?', button: 'Try again'}, 0);
        }
      }
    }
  }
  trackByFn(index, item) {
    return index;
  }
}
