import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-enter-name-dialog',
  templateUrl: './enter-name-dialog.component.html',
  styleUrls: ['./enter-name-dialog.component.css']
})
export class EnterNameDialogComponent implements OnInit {

  username;

  constructor(public dialogRef: MatDialogRef<any>) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
  }

}
