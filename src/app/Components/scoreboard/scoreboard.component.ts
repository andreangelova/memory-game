import { Component, OnInit } from '@angular/core';
import {AppStore} from '../../Models/AppStore.model';
import {Store} from '@ngrx/store';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit {

  scoreBoard;

  constructor(private store: Store<AppStore>, private spinner: NgxSpinnerService) {
    this.store.select(res => res.scoreBoard).subscribe(res => {
      res.loading ? this.spinner.show() : this.spinner.hide();
      if (res.scoreBoard) {
        this.scoreBoard = res.scoreBoard;
      }
    });
  }

  ngOnInit() {
  }

}
