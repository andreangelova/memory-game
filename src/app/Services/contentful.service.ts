import {Injectable} from '@angular/core';
import * as contenful from 'contentful';
import {environment} from '../../environments/environment';


@Injectable()
export class ContentfulService {
  client: any;


  constructor() {
    this.client = this.createClient();
  }

  createClient() {
    return contenful.createClient({
      space: environment.contentful.contentfulSpace,
      accessToken: environment.contentful.contentfulAccessToken,
      environment: environment.contentful.contentfulEnvironment
    });
  }

  getAssets(id) {
    return new Promise((resolve, reject) => {
      this.client.getAsset(id).then(
        asset => resolve({id: id, contentfulImg: 'https:' + asset.fields.file.url}),
        error => reject(error)
      );
    });
  }

}
