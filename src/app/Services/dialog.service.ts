import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {InfoDialogComponent} from '../Components/dialogs/info-dialog/info-dialog.component';
import {AppStore} from '../Models/AppStore.model';
import {Store} from '@ngrx/store';
import {StartGame} from '../Store/Game/game.action';
import {EnterNameDialogComponent} from '../Components/dialogs/enter-name-dialog/enter-name-dialog.component';
import {UpdateScoreBoard} from '../Store/ScoreBoard/scoreBoard.action';
import {Router} from '@angular/router';

@Injectable()
export class DialogSerivce {

  constructor(private store: Store<AppStore>, private dialog: MatDialog, private router: Router) {}

  openDialogAndAfterCloseStartGame(text, level) {
    const dialog = this.dialog.open(InfoDialogComponent, {
      data: {
        modalHeader: text.header,
        modalBodyMessage: text.body,
        modalButton: text.button
      }
    });
    dialog.afterClosed().subscribe(res => this.store.dispatch(new StartGame(level)));
  }

  openDialogEnterUserName(score) {
    const dialog = this.dialog.open(EnterNameDialogComponent);
    dialog.afterClosed().subscribe(res => {
      this.store.dispatch(new UpdateScoreBoard({'username': res, 'score': score}));
      this.store.dispatch(new StartGame(0));
      this.router.navigate(['scoreboard']);
    });
  }
}
