import {Injectable} from '@angular/core';

@Injectable()
export class GameSerivce {

  constructor() {}

  updateGame(data) {
    const row = data.row;
    const colum = data.colum;
    const openTile = this.findOpenTile(data.game);
    return new Promise(resolve => {
      data.game[row][colum]['open'] = true;
      if (!openTile) { // opening the first one
        resolve({game: data.game, score: data.score});
      } else { // opening the second one
        setTimeout(function () {
          if ((data.game[row][colum].img === openTile.img && data.game[row][colum].img) ||
            (data.game[row][colum].id === openTile.id && data.game[row][colum].id)) {
            data.game[row][colum]['match'] = openTile['match'] = true;
          } else if (data.score !== 0) {
            data.score--;
          }
          data.game[row][colum]['open'] = openTile['open'] = false;
          resolve({game: data.game, score: data.score});
        }, 1000);
      }
    });
  }

  findOpenTile(array) {
    for (let i = 0; i < array.length; i ++) {
      for (let j = 0; j < array[i].length; j++) {
        if (array[i][j]['open']) {
          return array [i][j];
        }
      }
    }
    return null;
  }

  checkIfGameIsDone(array) {
    for (let i = 0; i < array.length; i ++) {
      for (let j = 0; j < array[i].length; j ++) {
        if (!array[i][j]['match']) {
          return false;
        }
      }
    }
    return true;
  }
}
