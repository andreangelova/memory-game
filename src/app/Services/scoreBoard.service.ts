import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable()
export class ScoreBoardSerivce {

  constructor(private http: HttpClient, private afs: AngularFireStorage) {}

  getScoreBoard(url) {
   // return this.http.get(url);
    return new Promise((resolve, reject) => {
      const XMLHttp = new XMLHttpRequest();
      XMLHttp.open('GET', url, true);
      XMLHttp.send(null);
      XMLHttp.onreadystatechange = function() {
        if (XMLHttp.readyState === 4 && XMLHttp.status === 200) {
          resolve(XMLHttp.responseText);
        } else if (XMLHttp.readyState === 4 && XMLHttp.status !== 200) {
          reject(XMLHttp);
        }
      };
    });
  }

  updateScoreBoard(data) {
    if (data.scoreBoard[data.scoreBoard.length - 1].score < data.score || data.scoreBoard.length < 10) {
      data.scoreBoard = this.calculateScoreBoard(data.scoreBoard, data);
      const ref = this.afs.storage.ref('scores.json');
      data.scoreBoard = JSON.stringify(data.scoreBoard);
      return new Promise((resolve, reject) => {
        ref.putString(data.scoreBoard)
          .then(value => resolve(data.scoreBoard))
          .catch(error => reject(error));
      });
    } else { // the user is not in the top 10
      return new Promise(resolve => {
          resolve(JSON.stringify(data.scoreBoard));
      });
    }
  }

  calculateScoreBoard(array, data) {
    array.push({'username': data.username, 'score': data.score});
    array = array.sort((a, b) => b.score - a.score);
    if (array.length > 10) {
      array.pop(); // remove the last one
    }
    return array;
  }
}
