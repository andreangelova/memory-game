import { Component } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import {AppStore} from './Models/AppStore.model';
import {Store} from '@ngrx/store';
import {ListScoreBoard} from './Store/ScoreBoard/scoreBoard.action';
import {AddImage} from './Store/Game/game.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  loadOnce;

  constructor(private afs: AngularFireStorage, private store: Store<AppStore>) {
    const ref = this.afs.storage.ref('scores.json');
    const self = this;
    ref.getDownloadURL().then(function(url) { // get the data from the beginning
      self.store.dispatch(new ListScoreBoard(url));
    }).catch(function(error) {});

    this.store.select(res => res.games).subscribe(res => {
      const contentfulImages = res.contentfulImages;
      if (res.activeGame && !res.activeGame.game[0][0].img && res.activeGame.game[0][0].id && !this.loadOnce) {
        this.loadOnce = true;
        for (let i = 0; i < res.activeGame.game.length; i ++) {
          for (let j = 0; j < (<any>res.activeGame.game[i]).length; j++) {
            if (!contentfulImages.find(imageData => imageData.id === res.activeGame.game[i][j].id)) {
              // check if the image already exists, don't request it 2 times
              this.store.dispatch(new AddImage(res.activeGame.game[i][j].id));
            }
          }
        }
      }
    });
  }

}
